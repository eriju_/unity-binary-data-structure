﻿using Eriju.UnityBinaryDataStructure.Structure;
using System.IO;

namespace Eriju.UnityBinaryDataStructure.Elements {
	public class BDSUShort : BDSBase {
		public ushort Value;

		public BDSUShort() {

		}

		public BDSUShort(string key, ushort value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.USHORT;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadUInt16();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
