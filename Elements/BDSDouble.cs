﻿using Eriju.UnityBinaryDataStructure.Structure;
using System.IO;

namespace Eriju.UnityBinaryDataStructure.Elements {
	public class BDSDouble : BDSBase {
		public double Value;

		public BDSDouble() {

		}

		public BDSDouble(string key, float value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.DOUBLE;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadDouble();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
